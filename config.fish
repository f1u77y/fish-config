my_fish_add_to_path ~/.local/bin
my_fish_add_to_path ~/.cargo/bin

if test -d ~/.gem/ruby
    for ruby_version in (ls -1 ~/.gem/ruby)
        my_fish_add_to_path ~/.gem/ruby/$ruby_version/bin
    end
end

set -g fish_prompt_pwd_dir_length 0

set -gx DOTNET_CLI_TELEMETRY_OPTOUT 1
set -gx DOTNET_SKIP_FIRST_TIME_EXPERIENCE 1
set -gx PASSWORD_STORE_ENABLE_EXTENSIONS true

if status --is-interactive; and test -z "$SSH_AUTH_SOCK"
    eval (ssh-agent -c) > /dev/null 2> /dev/null
    trap "kill -9 $SSH_AGENT_PID" EXIT
end

if command -sq nvim
    set -gx EDITOR nvim
    set -gx VISUAL nvim
else
    set -gx EDITOR vim
    set -gx VISUAL vim
end

if command -sq col; and command -sq bat
    set -gx MANPAGER "sh -c 'col -bx | bat -l man -p'"
end

abbr --add gcl 'git clone'
abbr --add gco 'git checkout'
abbr --add gst 'git status'
set -g __fish_git_prompt_informative_status true
set -g __fish_git_prompt_showstashstate true
set -g __gist_git_prompt_showupstream auto,informative

set -g __fish_man_page_commands_with_subcommands_list ip git docker
set -g __fish_man_page_subcommands_only_list sudo
