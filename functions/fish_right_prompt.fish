# Defined in /tmp/fish.U1YgQI/fish_right_prompt.fish @ line 2
function fish_right_prompt
    set -l last_status $status
    set_color yellow
    # printf "%s " (__fish_git_prompt)
    printf "%s " (__fish_svn_prompt)
    if [ $last_status -ne 0 ]
        set_color $fish_color_error
        printf "%s" $last_status
        set_color $fish_color_normal
    end
end
