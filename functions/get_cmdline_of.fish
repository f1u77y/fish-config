function get_cmdline_of
    cat "/proc/$argv[1]/cmdline" | tr (printf "%s" '\0') ' '
end
