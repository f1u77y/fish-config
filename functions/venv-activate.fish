# Defined in /tmp/fish.o2Xj8E/venv-activate.fish @ line 2
function venv-activate
	set -l src "$argv[1]/bin/activate.fish"
    if test -f "$src"
        source "$src"
    else
        echo "No such venv: '$argv[1]'"
    end
end
