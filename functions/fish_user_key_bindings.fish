function fish_user_key_bindings
    bind \es my_fish_toggle_sudo
    bind \eh my_fish_man_page
    bind -k f1 my_fish_man_page
end

