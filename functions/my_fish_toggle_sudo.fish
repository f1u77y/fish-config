function my_fish_toggle_sudo
    set -l has_sudo 'false'
    if [ (string sub --length=5 -- (commandline)) = 'sudo ' ]
        set has_sudo 'true'
    else if test (string sub --length=8 -- (commandline)) = 'sudoedit'
        set has_sudo 'true'
    end
    if test $has_sudo = 'true'
        my_fish_remove_sudo
    else
        my_fish_add_sudo
    end
end
