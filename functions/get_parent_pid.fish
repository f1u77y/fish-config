function get_parent_pid
    ps -ef | awk "\$2 == $fish_pid { print \$3 }"
end
