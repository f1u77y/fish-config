# Defined in /tmp/fish.dGsUx3/my_fish_man_page.fish @ line 2
function my_fish_man_page
	set -l commandline (commandline -po)
    if test (count $commandline) = 0
        printf \a
        return 0
    end
    set -l command (basename $commandline[1])
    if test '(' (count $commandline) -lt 2 ')' -o '(' (string sub --length=1 -- "$commandline[2]") = '-' ')'
        command man $commandline[1] ^/dev/null
        or printf \a
        return 0
    end
    set -l subcommand $commandline[2]
    set -l man_page ""
    for command_with_subcommand in $__fish_man_page_commands_with_subcommands_list
        if test '(' "$command" = "$command_with_subcommand" ')' -a '(' -n "$subcommand" ')'
            command man "$command-$subcommand" ^ /dev/null
            or printf \a
            return 0
        end
    end
    for subcommand_only in $__fish_man_page_subcommands_only_list
        if test '(' "$command" = "$subcommand_only" ')' -a '(' -n "$subcommand" ')'
            command man "$subcommand" ^ /dev/null
            or printf \a
            return 0
        end
    end
    command man "$command" ^ /dev/null
    or eval "$command --help" | less -R
	or printf \a
end
