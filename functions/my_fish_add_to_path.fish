# Defined in /tmp/fish.Ea5Un0/my_fish_add_to_path.fish @ line 2
function my_fish_add_to_path
    if test ! -d "$argv[1]"
        return
    end
    set -l new_dir (realpath "$argv[1]")
    for dir in $PATH
        if [ "$dir" = "$new_dir" ]
            return
        end
    end
    set -gx PATH "$new_dir" $PATH
end
