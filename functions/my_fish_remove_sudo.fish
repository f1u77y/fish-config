# Defined in /tmp/fish.Ehwapv/fish_remove_sudo.fish @ line 2
function my_fish_remove_sudo
	set -l commandline (commandline -po; echo)
    set -l is_editor 'false'
    if test (count $commandline) -lt 1
        set is_editor 'false'
    else if test "$commandline[1]" = 'sudoedit'
        set is_editor 'true'
    end
    if test $is_editor = 'true'
        set -l editor_len (string length "$EDITOR")
        set -l cursor_pos (math (commandline -C) - 8 + $editor_len)
        set -l args (string sub --start=9 -- (commandline))
        commandline -r -- "$EDITOR$args"
        commandline -C "$cursor_pos"
    else
        set -l cursor_pos (math (commandline -C) - 5)
        set -l new_commandline (string sub --start=6 -- (commandline))
        commandline -r -- "$new_commandline"
        commandline -C "$cursor_pos"
    end
end
