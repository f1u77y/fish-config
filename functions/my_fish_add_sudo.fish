function my_fish_add_sudo
    set -l commandline (commandline -po; echo)
    set -l is_editor 'false'
    if test (count $commandline) -lt 1
        set is_editor 'false'
    else if test "$commandline[1]" = "$EDITOR"
        set is_editor 'true'
    else
        set is_editor 'false'
    end
    if test "$is_editor" = 'true'
        set -l editor_len (string length "$EDITOR")
        set -l cursor_pos (math (commandline -C) + 8 - $editor_len)
        set -l args (string sub --start=(math $editor_len + 1) (commandline))
        commandline -r -- "sudoedit$args"
        commandline -C "$cursor_pos"
    else
        set -l cursor_pos (math (commandline -C) + 5)
        commandline -C 0
        commandline -i 'sudo '
        commandline -C $cursor_pos
    end
end
