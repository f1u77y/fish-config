function fish_prompt
    set_color bryellow
    printf "%s@%s " "$USER" (hostname)
    set_color cyan
    printf "%s" (prompt_pwd)
    set_color green
    printf "%s" (__fish_git_prompt)
    set_color normal
    printf '\n'
    set_color cyan
    printf '%s' ' $ '
    set_color normal
end
